package mod_classes;

import java.util.ArrayList;

import com.badlogic.gdx.utils.Array;
import com.interrupt.dungeoneer.entities.Entity;
import com.interrupt.dungeoneer.entities.Item;
import com.interrupt.dungeoneer.entities.Player;
import com.interrupt.dungeoneer.entities.items.Wand;
import com.interrupt.dungeoneer.game.Game;
import com.interrupt.dungeoneer.overlays.OverlayManager;

import mod_classes.gui.WandShop;

public class WandGal extends BountifulNPC {
	public static java.util.List<Wand> wands;

	public WandGal() {
		super();
		// Set the gui type so the parent can instantiate it.
		gui = WandShop.class;
	}

	@Override
	public void onTrigger(Entity instigator, String value) {
		// Reset wand list if not set already.
		wands = new ArrayList<Wand>();
		openShop = true;
		Array<String> message = new Array<String>();
		Player player = Game.instance.player;
		// Search the players inventory and collect all the wands.
		for (Item item : player.inventory) {
			if (item instanceof Wand) {
				wands.add((Wand) item);
			}
		}
		// Set the message.
		if (player.gold == 0) {
			message.add("Begging will get you nowhere.");
			openShop = false;
		} else if (wands.size() > 0) {
			message.add("You look like you could use some help.");
			message.add("I can recharge your wands.. for a price!");
		} else {
			message.add("I can't help you...");
			openShop = false;
		}
		// Display the message GUI.
		OverlayManager.instance.push(new ShopMessage(message));
	}
}
