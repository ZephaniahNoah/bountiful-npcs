package mod_classes;

import java.lang.reflect.Field;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.interrupt.dungeoneer.entities.Entity;
import com.interrupt.dungeoneer.entities.Entity.ArtType;
import com.interrupt.dungeoneer.entities.Npc;
import com.interrupt.dungeoneer.entities.triggers.Trigger;
import com.interrupt.dungeoneer.game.Level;
import com.interrupt.dungeoneer.gfx.drawables.DrawableSprite;
import com.zephaniahnoah.delver.ModEventListener;

public class BountifulNPCEvents extends ModEventListener {

	@Override
	public void levelGenEvent(Level l) {
		if (l.levelName.equals("The Breach")) {
			addNpc(new ShopBro(), l, "shop_bro", 16.5f, 0, 22);
			addNpc(new WandGal(), l, "wand_gal", 15.5f, 0.15f, 23.5f);
		} else if (l.levelName.equals("The Well")) {
			addNpc(new ShopBro(), l, "shop_bro", 16.5f, 0, 13.5f);
		}
	}

	private void addNpc(Npc npc, Level l, String id, float x, float y, float z) {
		try {
			Field f = Level.class.getDeclaredField("static_entities");
			f.setAccessible(true);
			Array<Entity> static_entities = (Array<Entity>) f.get(l);

			// Should be something unique for each entity added to the level
			String triggerID = id;

			// Make NPC
			npc.id = triggerID;
			npc.setPosition(x, z, y);
			npc.isActive = true;
			npc.tex = 0;
			npc.spriteAtlas = id;
			npc.drawable = new DrawableSprite(0, ArtType.entity);
			npc.drawable.refresh();
			npc.appearsDuringEndgame = false;

			static_entities.add(npc);

			f = Level.class.getDeclaredField("entities");
			f.setAccessible(true);
			Array<Entity> entities = (Array<Entity>) f.get(l);

			// Make trigger
			Trigger trigger = new Trigger();
			trigger.triggersId = triggerID;
			trigger.setPosition(x, z, y);
			trigger.collision = new Vector3(.3f, .3f, .85f);
			trigger.isSolid = true;
			trigger.useVerb = "SHOP";
			trigger.appearsDuringEndgame = false;
			trigger.floating = true; // TODO: Is this really needed?

			entities.add(trigger);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}