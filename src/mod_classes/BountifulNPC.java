package mod_classes;

import java.util.Random;

import com.badlogic.gdx.utils.Array;
import com.interrupt.dungeoneer.entities.Npc;
import com.interrupt.dungeoneer.overlays.MessageOverlay;
import com.interrupt.dungeoneer.overlays.OverlayManager;

import mod_classes.gui.GUI;

public class BountifulNPC extends Npc {

	// Stores the GUI class type for instantiation.
	public Class<? extends GUI> gui;
	// Stores the value which determines whether or not the shop GUI should open.
	protected static boolean openShop = true;
	// Randomizer
	protected static Random rand = new Random();
	// An instance of this NPC
	private Npc instance;

	public BountifulNPC() {
		super();
		instance = this;
	}

	// Internal class for the message GUI.
	// We make our own message class so we can open the shop GUI when the massage hides.
	protected class ShopMessage extends MessageOverlay {
		public ShopMessage(Array<String> message) {
			super(message, null, null);
			this.pausesGame = false;
		}

		@Override
		public void onHide() {
			// If the shop GUI should be opened.
			if (openShop) {
				// Try to open the shop GUI!
				try {
					GUI Gui = gui.newInstance();
					OverlayManager.instance.push(Gui);
					Gui.npc = instance;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
