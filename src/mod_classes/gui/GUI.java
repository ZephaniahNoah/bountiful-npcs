package mod_classes.gui;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.interrupt.dungeoneer.Art;
import com.interrupt.dungeoneer.entities.Entity;
import com.interrupt.dungeoneer.entities.Npc;
import com.interrupt.dungeoneer.gfx.TextureAtlas;
import com.interrupt.dungeoneer.overlays.WindowOverlay;

public class GUI extends WindowOverlay {

	// Load the art for the window and tooltip. Or... grab it from the cache in reality.
	protected final Texture windowTexture = Art.loadTexture("ui/window.png");
	protected final Texture tooltipTexture = Art.loadTexture("ui/tooltip.png");
	protected final TextureAtlas itemTextures = TextureAtlas.getCachedRegion(Entity.ArtType.item.toString());

	// The NPC which the GUI was created from.
	public Npc npc;

	public GUI() {
		this.pausesGame = false;
	}

	@Override
	public Table makeContent() {
		return null;
	}
}