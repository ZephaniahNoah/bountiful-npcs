package mod_classes.gui;

import org.lwjgl.input.Mouse;

import com.badlogic.gdx.graphics.Color;
import com.interrupt.dungeoneer.Audio;
import com.interrupt.dungeoneer.GameManager;
import com.interrupt.dungeoneer.entities.Item;
import com.interrupt.dungeoneer.entities.Player;
import com.interrupt.dungeoneer.entities.items.ItemStack;
import com.interrupt.dungeoneer.game.Game;
import com.interrupt.dungeoneer.gfx.TextureAtlas;

import mod_classes.ShopBro;

public class SellShop extends GUI {

	@Override
	public void draw(final float delta) {
		this.renderer = GameManager.renderer;
		final float uiSize = Game.GetUiSize();
		renderer.uiBatch.begin();
		renderer.uiBatch.enableBlending();

		float width = uiSize * 14;
		float height = uiSize * 7;
		float wx = -(width / 2);
		float wy = -(height / 2);

		// Draw the back window
		renderer.uiBatch.setColor(Color.WHITE);
		renderer.uiBatch.draw(windowTexture, wx, wy, width, height);

		Player player = Game.instance.player;

		int count = player.inventory.size;
		int drawn = 0;

		double tilesWidth = 10D;
		int tilesHeight = (int) Math.ceil(((double) count / tilesWidth));

		// Get the game window size
		float hx = -renderer.camera2D.viewportWidth / 2.0f;
		float hy = -renderer.camera2D.viewportHeight / 2.0f;

		float size = (uiSize + (uiSize / 80.0f));

		// Draw the player's gold.
		renderer.drawText("Your gold: " + player.gold, wx + uiSize * 2, wy + uiSize, uiSize * .2f, Color.YELLOW);

		// Draw shop bro's gold.
		renderer.drawText("Gold: " + ((ShopBro) this.npc).gold, wx + uiSize * 2, wy + height - (uiSize + uiSize / 4), uiSize * .2f, Color.YELLOW);

		Item toolTipItem = null;
		float tenthOfScale = uiSize / 10f;
		float fontSize = uiSize * .16f;
		float bigFont = (fontSize * 1.4f);
		float numberOffset = uiSize - bigFont - tenthOfScale;

		// Loop through inventory slots.
		// Lots of math is done to scale with the UI Size and center the slots even with inventory expansions.
		for (int y = tilesHeight - 1; y > -1; y--) {
			float length = size * (float) tilesWidth;
			int remaining = count - drawn;
			int amount = (int) tilesWidth;
			if (remaining < tilesWidth) {
				length = (float) remaining * size;
				amount = remaining;
			}
			float offset = (length / 2);
			for (int x = 0; x < amount; x++) {
				float ix = x * size - offset;
				float iy = (y * size) - ((tilesHeight * size) / 2);
				float r = 1;
				float g = 1;
				float b = 1;
				float a = .2f;

				// Get the item.
				Item item = Game.instance.player.inventory.get(drawn);

				// If the mouse is over a slot.
				if (Mouse.getX() + hx > ix && Mouse.getX() + hx < ix + uiSize) {
					if (Mouse.getY() + hy > iy && Mouse.getY() + hy < iy + uiSize) {
						a = 0.45f;
						toolTipItem = item;
					}
				}

				// If is item is being held by the player.
				if (item != null && item == player.GetHeldItem()) {
					// Reduce green and blue to make the slot texture more red.
					g = .11f;
					b = .11f;
				}

				// Draw the slot texture.
				renderer.uiBatch.setColor(new Color(r, g, b, a));
				renderer.uiBatch.draw(itemTextures.getSprite(127), ix, iy, uiSize, uiSize);
				drawn++;

				// If the slot is empty.
				if (item == null)
					continue;

				// Grab the item texture.
				final String iconAtlas = (item.spriteAtlas != null) ? item.spriteAtlas : "item";
				final TextureAtlas t = TextureAtlas.cachedAtlases.get(iconAtlas);

				// If the texture is missing then skip it.
				// Usually this happens when the item is from a missing mod.
				if (t == null)
					continue;

				// Draw the item
				renderer.uiBatch.setColor(Color.WHITE);
				renderer.uiBatch.draw(t.getSprite(item.tex), ix, iy, uiSize, uiSize);

				// If the item is a stack of items.
				if (item instanceof ItemStack) {
					// Draw the item count.
					renderer.drawText(((ItemStack) item).count + "", ix + numberOffset, iy + numberOffset, bigFont, Color.WHITE);
				}
			}
		}

		// If the item in the slot under the mouse is not null.
		if (toolTipItem != null) {
			// Draw the tooltip background.
			renderer.uiBatch.setColor(1f, 1f, 1f, .8f);
			renderer.uiBatch.draw(tooltipTexture, Mouse.getX() + hx, Mouse.getY() + hy, uiSize, uiSize);

			// Set the cost to 45% of the original cost.
			int newCost = (int) (toolTipItem.cost * .45);

			// If the item is a stack of items.
			if (toolTipItem instanceof ItemStack) {
				// Reduce the cost even more. Then multiply by the amount of items.
				newCost *= (((ItemStack) toolTipItem).count * .6);
			}

			// Self explanatory. Don't let the cost be nothing.
			if (newCost == 0) {
				newCost = 1;
			}

			// If the mouse clicked on any item that isn't the held item. And if shop bro can afford the item.
			if (Mouse.isButtonDown(0) && toolTipItem != player.GetHeldItem() && ((ShopBro) this.npc).gold >= newCost) {
				// Add gold.
				player.gold += newCost;
				// Remove item.
				player.removeFromInventory(toolTipItem);
				// Play sound.
				Audio.playSound("ui/ui_buy.mp3", 0.6f);
				// Remove gold from NPC
				((ShopBro) this.npc).gold -= newCost;
			}

			// Draw the cost of the item onto the tooltip.
			renderer.drawText(newCost + "", Mouse.getX() + hx + tenthOfScale, Mouse.getY() + hy + ((uiSize - fontSize) - tenthOfScale * 2), fontSize, Color.YELLOW);
			renderer.uiBatch.setColor(Color.WHITE);
			// Draw the gold texture onto the tooltip.
			renderer.uiBatch.draw(itemTextures.getSprite(89), Mouse.getX() + hx, Mouse.getY() + hy + tenthOfScale, uiSize, uiSize);
		}
		renderer.uiBatch.end();
	}
}