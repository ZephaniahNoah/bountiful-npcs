package mod_classes.gui;

import org.lwjgl.input.Mouse;

import com.badlogic.gdx.graphics.Color;
import com.interrupt.dungeoneer.Audio;
import com.interrupt.dungeoneer.GameManager;
import com.interrupt.dungeoneer.entities.Player;
import com.interrupt.dungeoneer.entities.items.Wand;
import com.interrupt.dungeoneer.game.Game;
import com.interrupt.dungeoneer.gfx.TextureAtlas;
import com.interrupt.dungeoneer.ui.FontBounds;

import mod_classes.WandGal;

public class WandShop extends GUI {

	// Flipper for edge trigger. How else do I explain this thingy?
	// It's for treating a mouse press as a single quick click and doesn't trigger again until the mouse is released then pressed again.
	// Oof.. confusing.
	private boolean pressed = false;

	@Override
	protected void draw(float delta) {
		// A lot of the stuff done here is similar how SellShop.java does it.
		this.renderer = GameManager.renderer;
		final float uiSize = Game.GetUiSize();
		Player player = Game.instance.player;

		float width = uiSize * 14;
		float height = uiSize * 7;
		float wx = -(width / 2);
		float wy = -(height / 2);

		renderer.uiBatch.begin();

		renderer.uiBatch.setColor(Color.WHITE);
		renderer.uiBatch.draw(windowTexture, wx, wy, width, height);

		int wandCount = WandGal.wands.size();
		double rowLength = 10D;
		int rowHeight = (int) Math.ceil(wandCount / rowLength);
		int drawn = 0;

		renderer.drawText("Gold: " + player.gold, wx + uiSize * 2, wy + uiSize, uiSize * .2f, Color.YELLOW);

		float hx = -renderer.camera2D.viewportWidth / 2.0f;
		float hy = -renderer.camera2D.viewportHeight / 2.0f;

		Wand wandUnderMouse = null;

		// Loop for drawing slots and items in their slots.
		loop: for (int y = rowHeight - 1; y > -1; y--) {
			int remaining = wandCount - drawn;

			double length = rowLength;

			if (remaining < rowLength) {
				length = remaining;
			}
			for (int x = 0; x < rowLength; x++) {
				if (drawn < wandCount) {

					float ix = (x * uiSize - (float) ((length * uiSize) / 2));
					float iy = (y * uiSize) - (rowHeight * uiSize) / 2;

					float alpha = .2f;

					Wand wand = WandGal.wands.get(drawn);

					if (Mouse.getX() + hx > ix && Mouse.getX() + hx < ix + uiSize && Mouse.getY() + hy > iy && Mouse.getY() + hy < iy + uiSize) {
						alpha = .45f;
						wandUnderMouse = wand;
					}

					// Draw slot.
					renderer.uiBatch.setColor(new Color(1f, 1f, 1f, alpha));
					renderer.uiBatch.draw(itemTextures.getSprite(127), ix, iy, uiSize, uiSize);

					// Draw item in that slot.
					final String iconAtlas = (wand.spriteAtlas != null) ? wand.spriteAtlas : "item";
					final TextureAtlas t = TextureAtlas.cachedAtlases.get(iconAtlas);
					renderer.uiBatch.setColor(Color.WHITE);
					renderer.uiBatch.draw(t.getSprite(wand.tex), ix, iy, uiSize, uiSize);

					drawn++;
				} else {
					break loop;
				}
			}
		}

		if (wandUnderMouse != null) {
			float fontSize = uiSize * .16f;
			float padding = uiSize / 10f;
			float size = uiSize * 1.25f;
			// A couple attempts to set a balanced price for adding a charge to a wand.
			// int cost = (int) Math.ceil((wandUnderMouse.cost / 28f + 1f) * wandUnderMouse.charges / 4.5f + 0.01f) + wandUnderMouse.getRandDamage() + 2;
			int cost = (int) (wandUnderMouse.charges / 1.7f + wandUnderMouse.getRandDamage() / 1.8f + wandUnderMouse.cost / 25f);
			renderer.uiBatch.setColor(1f, 1f, 1f, .8f);
			String message = "Charges: " + wandUnderMouse.charges;
			// A failed attempt to extend the tooltip background width to the length of the text within it.
			// TODO: Fix this...
			float toolTipWidth = FontBounds.GetBounds(renderer.font, message).width;
			renderer.uiBatch.draw(tooltipTexture, Mouse.getX() + hx, Mouse.getY() + hy, toolTipWidth * (uiSize * .025f), size);
			float x = Mouse.getX() + hx + padding;
			float y = Mouse.getY() + hy + ((size - fontSize) - padding * 2);
			renderer.drawText(message, x, y, fontSize, Color.WHITE);
			renderer.drawText("Cost: " + cost, x, y - fontSize * 1.1f, fontSize, Color.YELLOW);
			renderer.uiBatch.setColor(Color.WHITE);
			renderer.uiBatch.draw(itemTextures.getSprite(89), Mouse.getX() + hx, Mouse.getY() + hy + padding, uiSize, uiSize);
			if (Mouse.isButtonDown(0) && pressed == false && player.gold - cost >= 0) {
				player.gold -= cost;
				wandUnderMouse.charges += 1;
				Audio.playSound("ui/ui_buy.mp3", 0.6f);
			}
			if (Mouse.isButtonDown(0)) {
				pressed = true;
			} else {
				pressed = false;
			}
		}
		renderer.uiBatch.end();
	}
}