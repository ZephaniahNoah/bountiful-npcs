package mod_classes;

import com.badlogic.gdx.utils.Array;
import com.interrupt.dungeoneer.entities.Entity;
import com.interrupt.dungeoneer.entities.Item;
import com.interrupt.dungeoneer.entities.Player;
import com.interrupt.dungeoneer.game.Game;
import com.interrupt.dungeoneer.overlays.OverlayManager;

import mod_classes.gui.SellShop;

public class ShopBro extends BountifulNPC {

	// Arrays of random messages.
	private static final String[] lighten = { "Allow me to lighten your burden.", "Please, sell me something. Anything!", "You must sell me something!" };
	private static final String[] nothingLeft = { "Are you really sure you don't have anything!? Unfortunate..", "Empty! What an absolute shame..", "Completely run dry. It's a crime, really." };
	private static final String[] greet = { "Hello traveler! Got any spare wares?", "I'm looking for supplies.", "I require all the equipment!", "Someone sold me a nice sweetroll once." };
	private static final String[] loaded = { "Woah! That's a lot of stuff!", "Not a single empty pocket." };

	public int gold = 1200;

	// Takes a string array and returns a random message.
	private static String randomString(String[] input) {
		return input[rand.nextInt(input.length)];
	}

	public ShopBro() {
		super();
		// Set the gui type so the parent can instantiate it.
		gui = SellShop.class;
	}

	@Override
	public void onTrigger(final Entity instigator, final String value) {
		super.onTrigger(instigator, value);

		Array<String> messages = new Array<String>();
		Player player = Game.instance.player;

		if (player.hasFreeInventorySpace()) {
			boolean itemFound = false;
			boolean skullFound = false;
			openShop = true;
			// Loop through the player's inventory.
			for (Item i : player.inventory) {
				// Make sure the slot isn't empty.
				if (i != null) {
					// Try to detect if an item is a skull by checking it's name.
					// & make sure we didn't already find a skull to prevent showing the skull message twice.
					if (i.name.toLowerCase().equals("skull") && !skullFound) {
						messages.add("Is that a skull? Can I have it?!?");
						skullFound = true;
					}
					// An item was found so set found to true.
					itemFound = true;
				}
			}
			// If an items was found but no skull was found.
			if (itemFound) {
				if (!skullFound) {
					// Add a random greeting message and a random standard message.
					messages.add(randomString(greet));
					messages.add(randomString(lighten));
				}
			} else {
				// If no items were found then add a message respectively.
				messages.add(randomString(nothingLeft));
				// Set openShop to false. No need to open the shop if there's nothing to sell.
				openShop = false;
			}
		} else {
			// The player has no free inventory slots.
			messages.add(randomString(loaded));
			messages.add(randomString(lighten));
		}

		// Finally display the message GUI.
		OverlayManager.instance.push(new ShopMessage(messages));
	}
}
